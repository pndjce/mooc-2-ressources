# Une activité complète sur plusieurs séances

Charles Poulmaire a préparé un cours complet niveau Première NSI, pour introduire les langages du web. 

Cette activité est constituée de plusieurs fiches à destination des élèves.

- 2 fiches de travaux pratiques :
    - [TP 1](https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/2_Mettre-en-oeuvre-Animer/Web_statique_TP1.pdf)
    - [TP 2](https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/2_Mettre-en-oeuvre-Animer/Web_statique_TP2.pdf)

- [Le résumé de cours HTML5-CSS3 au format PDF](https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/2_Mettre-en-oeuvre-Animer/Web_statique_cours.pdf) qui sera donné après les 2 TP. 

Dans un premier temps, nous pouvons nous approprier cette activité en regardant le document suivant. 
La [fiche _prof_ de l'activité](https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/2_Mettre-en-oeuvre-Animer/HTML_CSS_JS_fiche_prof.md) contenant une liste de méta-informations sur ce cours.

Dans un deuxième temps, en lisant l'[analyse de l'activité par l'auteur](https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/2_Mettre-en-oeuvre-Animer/HTML_CSS_JS_fiche_analyse.md) 

On retrouve sur le forum [la discussion autour de cette activité](https://mooc-forums.inria.fr/moocnsi/moocnsi/t/analyse-introduction-aux-langages-du-web/3710).
